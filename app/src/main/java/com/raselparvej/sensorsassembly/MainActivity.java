package com.raselparvej.sensorsassembly;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.hardware.SensorManager;
import android.hardware.Sensor;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Google Ads
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;



public class MainActivity extends AppCompatActivity {

    private AdView adBannerHead;
    private ListView sensorListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                adBannerHead = findViewById(R.id.adBannerHead);
                AdRequest adRequest = new AdRequest.Builder().build();
                adBannerHead.loadAd(adRequest);
            }
        });



        SensorManager sMgr = (SensorManager)this.getSystemService(SENSOR_SERVICE);
        List<Sensor> sensors = sMgr.getSensorList(Sensor.TYPE_ALL);
        final List<Sensor> sensorList = new ArrayList<Sensor>();
        sensorList.addAll(sensors);
        Collections.sort(sensorList, new SensorNameComparator());


        sensorListView = (ListView) findViewById(R.id.sensorListView);

        ArrayAdapter<Sensor> adapter = new MyArrayAdapter(this, sensorList);

        sensorListView.setAdapter(adapter);

        sensorListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                Sensor sensor = sensorList.get(position);
                String sensorInformation = "Sensor No (In Alphabetic Order) : "
                        + String.valueOf(position+1)
                        +"\nName : "
                        + sensor.getName()
                        + "\nType : "
                        + (Build.VERSION.SDK_INT >= 20 ? sensor.getStringType()
                        : "Your Device's Android API Level is not Enough to Retrieve This Information")
                        + "\nVendor : "
                        + sensor.getVendor()
                        + "\nVersion : "
                        + String.valueOf(sensor.getVersion())
                        + "\nResolution : "
                        + String.valueOf(sensor.getResolution())
                        + "\nIs Dynamic Sensor? : "
                        + (Build.VERSION.SDK_INT >= 24 ? String.valueOf(sensor.isDynamicSensor()? "Yes" : "No")
                        : "Your Device's Android API Level is not Enough to Retrieve This Information")
                        + "\nIs WakeUP Sensor? : "
                        + (Build.VERSION.SDK_INT >= 24 ? String.valueOf(sensor.isWakeUpSensor()? "Yes" : "No")
                        : "Your Device's Android API Level is not Enough to Retrieve This Information")
                        + "\n";

                ClipData clip = ClipData.newPlainText("Sensor Information", sensorInformation);
                clipboard.setPrimaryClip(clip); //To clipboard



                Context context = getApplicationContext();
                CharSequence text = "Sensor Information is Copied to Your Device's Clipboard.";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();


                return true;
            }
        });


        Toast.makeText(getApplicationContext(), "Long Tap to Copy Sensor Information!", Toast.LENGTH_LONG).show();

    }
}
