package com.raselparvej.sensorsassembly;


import android.content.Context;
import android.hardware.Sensor;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.TextView;



import java.util.List;



public class MyArrayAdapter extends ArrayAdapter<Sensor> {

//    private ListRowViewHolder listRowViewHolder;

    public MyArrayAdapter(Context context, List<Sensor> sensorList) {
        super(context, 0, sensorList);
    }


    @Override
    public View getView(int position, View listRowView, ViewGroup parent) {

        View recycledListRowView = listRowView;

        if(recycledListRowView == null) {
            recycledListRowView = LayoutInflater.from(this.getContext()).inflate(R.layout.listview_item, parent, false);
        }

        Sensor sensor = this.getItem(position);

        ((TextView) recycledListRowView.findViewById(R.id.serial)).setText(String.valueOf(position+1));
        ((TextView) recycledListRowView.findViewById(R.id.name)).setText(sensor.getName());

        if(Build.VERSION.SDK_INT >= 20) {
            ((TextView) recycledListRowView.findViewById(R.id.type)).setText(sensor.getStringType());
        } else {
            ((TextView) recycledListRowView.findViewById(R.id.type)).setText("Your Device's Android API Level is not Enough to Retrieve This Information");
        }

        ((TextView) recycledListRowView.findViewById(R.id.vendor)).setText(sensor.getVendor());
        ((TextView) recycledListRowView.findViewById(R.id.version)).setText(String.valueOf(sensor.getVersion()));
        ((TextView) recycledListRowView.findViewById(R.id.resolution)).setText(String.valueOf(sensor.getResolution()));

        if(Build.VERSION.SDK_INT >= 24) {
            ((TextView) recycledListRowView.findViewById(R.id.dynamic)).setText(sensor.isDynamicSensor()?"Yes" : "No");
        } else {
            ((TextView) recycledListRowView.findViewById(R.id.dynamic)).setText("Your Device's Android API Level is not Enough to Retrieve This Information");
        }


        if(Build.VERSION.SDK_INT >= 24) {
            ((TextView) recycledListRowView.findViewById(R.id.wakeup)).setText(sensor.isWakeUpSensor()?"Yes" : "No");
        } else {
            ((TextView) recycledListRowView.findViewById(R.id.wakeup)).setText("Your Device's Android API Level is not Enough to Retrieve This Information");
        }








//        System.out.println("****************************************Position " + position);





        return recycledListRowView;

    };

}
