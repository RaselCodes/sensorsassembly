package com.raselparvej.sensorsassembly;

import android.hardware.Sensor;

import java.util.Comparator;


public class SensorNameComparator implements Comparator<Sensor> {
    public int compare(Sensor a, Sensor b)
    {
        return a.getName().compareToIgnoreCase(b.getName());
    }
}

